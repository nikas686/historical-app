import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App/App';
import * as serviceWorker from './serviceWorker';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import {ThemeProvider} from 'react-jss'

const theme = {
  colorDark: '#333333',
  colorWhite: '#ffffff'
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Router>
      <Switch>
        <Route path={'/:number?'} component={App}/>
      </Switch>
    </Router>
  </ThemeProvider>,
  document.getElementById('root')
)

serviceWorker.unregister();
