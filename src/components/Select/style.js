import arrow from '../../img/arrow.svg'

export const styles = theme => ({
  Select: {
    display: 'block',
    position: 'relative',
    margin: 0,
    padding: 0,
    width: 90,

    '& .select-wrap': {
      display: 'block',
      position: 'relative',
      margin: 0,
      padding: [0, 0, 0, 0],
      border: '1px solid #C4C4C4',
      background: theme.colorWhite,
      zIndex: '2',
      userSelect: 'none',
      cursor: 'pointer',

      '&.opened-true': {
        zIndex: 5,

        '& .placeholder-container::before': {
          transform: 'rotate(180deg)',
        },
      },

      '&.selected-true': {

        '& .clear-value': {
          display: 'block',
        },
      },

      '& .placeholder-container': {
        display: 'block',
        position: 'relative',
        margin: 0,
        padding: [7, 30, 7, 20],
        fontSize: '14px',
        lineHeight: '24px',
        color: '#646464',
        overflow: 'hidden',

        '& .select-item, & .placeholder': {
          overflow: 'hidden',
          height: '24px',
        },

        '& span': {
          display: 'inline-block',
          verticalAlign: 'baseline',
          margin: [0, 5, 0, 0],

        },
        '&::before': {
          content: '""',
          position: 'absolute',
          width: '16px',
          height: '18px',
          top: 0,
          bottom: 0,
          right: 10,
          margin: 'auto',
          transition: 'all linear .2s',
          background: {
            image: 'url(' + arrow + ')',
            position: 'center center',
            repeat: 'no-repeat',
            size: 'contain',
          },
        },

      },

      '&.error': {
        borderColor: '#EF5353 !important',

        '&:focus': {
          outlineColor: '#EF5353',
        },
      },

      '& .drop-down-container': {
        position: 'absolute',
        display: 'none',
        bottom: '100%',
        left: '-1px',
        right: '-1px',
        margin: [0, 0, '-1px', 0],
        padding: 0,
        border: '1px solid #C4C4C4',

        '& .select-item': {
          display: 'block',
          position: 'relative',
          margin: [0, 0, 0, 0],
          padding: [4, 20, 4, 20],
          fontSize: '14px',
          lineHeight: '24px',
          color: '#646464',
          background: theme.colorWhite,
          cursor: 'pointer',

          '&:hover, &.visible-true': {
            background: '#F2F2F2',
          },

          '& span': {
            display: 'inline-block',
            verticalAlign: 'baseline',
            margin: [0, 5, 0, 0],

            '&.fullName': {
              fontWeight: '600',
            },
          },
        },
      },

      '&.opened-true .drop-down-container': {
        display: 'block',
      },
    },

    '&.themeOptions': {
      flex: '0 0 83.3333333333%',
      maxWidth: '83.3333333333%',
      padding: [0, 15],
      boxSizing: 'border-box',
    },
  },
})
