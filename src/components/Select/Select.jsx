import React, {Component} from 'react'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'

import {styles} from './style'

export class Select extends Component {

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = event => {
    if (this.node && !this.node.contains(event.target)) {
      this.setState({isVisible: false})
    }
  };

  state = {
    isVisible: false,
  }

  isSelected = id => {
    return ((this.props.selected === id).toString())
  }

  toggleSelect = () => {
    this.setState({isVisible: !this.state.isVisible})
  }

  handleSelectClick = id => {
    this.setState({isVisible: !this.state.isVisible})
    this.props.onClick(id)
  }

  renderPlaceholder = () => {
    const { options, selected} = this.props
    const placeholderItem = options.filter(item => item.id === selected)
    return (
      <span className={'placeholder-item'}>{placeholderItem[0].name}</span>
    )
  }


  render() {
    const {
      classes,
      options,
    } = this.props

    return (
      <div className={classes.Select}>
        <div ref={node => this.node = node}
             className={'select-wrap opened-' + (this.state.isVisible).toString() + ' selected-' + (this.props.selected !== '').toString()}>
          <div className={'placeholder-container'} onClick={this.toggleSelect}>
            {this.renderPlaceholder()}
          </div>
          <div className="drop-down-container">
            {options.map((item, index) => (
              <div key={index + item.id} onClick={() => this.handleSelectClick(item.id)}
                   className={'select-item visible-' + this.isSelected(item.id)} id={item.id}>
                <span>{item.name}</span>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }

}

// Типизация переменных
Select.propTypes = {
  classes: PropTypes.object.isRequired,
  options: PropTypes.array,
  selected: PropTypes.any,
  onClick: PropTypes.func,
}

export default injectSheet(styles)(Select)
