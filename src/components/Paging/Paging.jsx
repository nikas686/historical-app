import React, { Component, Fragment } from 'react'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'

import { styles } from './style'

export class Paging extends Component {

  renderMidPage = (currentPage, lastPage) => {
    const { onClick } = this.props
    const numPage = Number(currentPage)
    if (numPage === 1 || numPage === lastPage) {
      return (
        <Fragment>
          <a
            className={'page'}
            name="link"
            id={1}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={numPage === 1}
          >
            1
          </a>
          <span>...</span>
          <a
            className={'page'}
            name="link"
            id={lastPage}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={numPage === lastPage}
          >
            {lastPage}
          </a>
        </Fragment>
      )
    } else if (numPage > 1 && numPage < lastPage / 2) {
      return (
        <Fragment>
          <a
            className={'page'}
            name="link"
            id={numPage - 1}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={false}
          >
            {numPage - 1}
          </a>

          <a
            className={'page'}
            name="link"
            id={numPage}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={true}
          >
            {numPage}
          </a>

          <a
            className={'page'}
            name="link"
            id={numPage + 1}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={false}
          >
            {numPage + 1}
          </a>
          <span>...</span>
          <a
            className={'page'}
            name="link"
            id={lastPage}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={numPage === lastPage}
          >
            {lastPage}
          </a>
        </Fragment>
      )
    } else if (numPage < lastPage && numPage >= lastPage / 2) {
      return (
        <Fragment>
          <a
            className={'page'}
            name="link"
            id={1}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={false}
          >
            {1}
          </a>
          <span>...</span>
          <a
            className={'page'}
            name="link"
            id={numPage - 1}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={false}
          >
            {numPage - 1}
          </a>

          <a
            className={'page'}
            name="link"
            id={numPage}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={true}
          >
            {numPage}
          </a>

          <a
            className={'page'}
            name="link"
            id={numPage + 1}
            onClick={event =>
              onClick(event.target.name, event.target.id)
            }
            aria-disabled={false}
          >
            {numPage + 1}
          </a>
        </Fragment>
      )
    }
  }


  render() {
    const {
      classes,
      data,
      currentPage,
      itemPerPage,
      onClick,
    } = this.props

    const total = data.length
    const lastPage = Math.ceil(total / itemPerPage)
    const isLast = Number(currentPage) === Number(lastPage)
    const isFirst = currentPage === '1'

    return (
      <div className={classes.Paging}>
        <a
          className={'btn first'}
          id={1}
          onClick={event =>
            onClick(event.target.name, event.target.id)
          }
          name="link"
          aria-disabled={isFirst}
        >
        </a>
        <a
          className={'prev-page nav'}
          id={currentPage - 1}
          onClick={event =>
            onClick(event.target.name, event.target.id)
          }
          name="prev"
          aria-disabled={isFirst}
        >
        </a>

        {this.renderMidPage(currentPage, lastPage)}

        <a
          className={'next-page nav'}
          id={Number(currentPage) + 1}
          onClick={event =>
            onClick(event.target.name, event.target.id)
          }
          name="next"
          aria-disabled={isLast}
        >
        </a>
        <a
          className={'btn last'}
          id={lastPage}
          onClick={event =>
            onClick(event.target.name, event.target.id)
          }
          name="link"
          aria-disabled={isLast}
        >
        </a>
      </div>
    )
  }

}

// Типизация переменных
Paging.propTypes = {
  classes: PropTypes.object,
  data: PropTypes.array,
  currentPage: PropTypes.number,
  itemPerPage: PropTypes.number,
  onClick: PropTypes.func,
}

export default injectSheet(styles)(Paging)
