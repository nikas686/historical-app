import arrow from '../../img/arrow.svg'
import dbl_arrow from '../../img/dbl_arrow.svg'

export const styles = {
  Paging: {

    '& a': {
      display: 'inline-block',
      verticalAlign: 'middle',
      textDecoration: 'none',
      opacity: 0.4,
      margin: [0, 10],

      '&[aria-disabled=false]': {
        cursor: 'pointer',
        opacity: 1,
      },

    },

    '& .nav': {
      width: '16px',
      height: '18px',
      margin: 0,
      transition: 'all linear .2s',
      background: {
        image: 'url(' + arrow + ')',
        position: 'center center',
        repeat: 'no-repeat',
        size: 'contain',
      },

      '&.prev-page': {
        transform: 'rotate(90deg)',
      },
      '&.next-page': {
        transform: 'rotate(-90deg)',
      },

    },

    '& .btn': {
      width: 15,
      height: 20,
      margin: [0, 0, 0, 10],
      transition: 'all linear 0.2s',
      background: {
        image: 'url(' + dbl_arrow + ')',
        position: 'center center',
        repeat: 'no-repeat',
        size: 'contain',
      },

      '&.first': {
        transform: 'rotate(180deg)',
        margin: [0, 10, 0, 0],
      },

    },

    '& span': {
      display: 'inline-block',
      verticalAlign: 'middle',

    },

  },
}
