import cross from '../../img/cross.svg'
import menu from '../../img/menu.svg'

export const style = theme => ({
  '@global': {
    body: {
      margin: 0,
      padding: 0,
      fontFamily: ['Roboto Slab'],
      fontSize: '14px',
      color: theme.colorDark,
      lineHeight: '16px',
      background: theme.colorWhite,
    },

    '.row': {
      display: 'flex',
      flexWrap: 'wrap',
      margin: [0, -15],

      '& .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12': {
        position: 'relative',
        width: '100%',
        minHeight: '1px',
        padding: [0, 15],
        boxSizing: 'border-box',
      },

      '& .col-1': {
        flex: '0 0 8.3333333333%',
        maxWidth: '8.3333333333%',
      },

      '& .col-2': {
        flex: '0 0 16.6666666667%',
        maxWidth: '16.6666666667%',
      },

      '& .col-3': {
        flex: '0 0 25%',
        maxWidth: '25%',
      },

      '& .col-4': {
        flex: '0 0 33.3333333333%',
        maxWidth: '33.3333333333%',
      },

      '& .col-5': {
        flex: '0 0 41.6666666667%',
        maxWidth: '41.6666666667%',
      },

      '& .col-6': {
        flex: '0 0 50%',
        maxWidth: '50%',
      },

      '& .col-7': {
        flex: '0 0 58.3333333333%',
        maxWidth: '58.3333333333%',
      },

      '& .col-8': {
        flex: '0 0 66.6666666667%',
        maxWidth: '66.6666666667%',
      },

      '& .col-9': {
        flex: '0 0 75%',
        maxWidth: '75%',
      },

      '& .col-10': {
        flex: '0 0 83.3333333333%',
        maxWidth: '83.3333333333%',
      },

      '& .col-11': {
        flex: '0 0 91.6666666667%',
        maxWidth: '91.6666666667%',
      },

      '& .col-12': {
        flex: '0 0 100%',
        maxWidth: '100%',
      },
    },
  },
  App: {
    background: '#FCFCFC',
    height: '100vh',
    display: 'flex',

    '& .sidebar': {
      width: '245px',
      background: theme.colorWhite,
      height: '100vh',
      boxSizing: 'border-box',
      boxShadow: '4px 0px 4px rgba(0, 0, 0, 0.05)',
      padding: [90, 0, 20, 0],
      position: 'relative',
      zIndex: 6,
      overflowY: 'auto',

      '& h2': {
        margin: [0, 0, 30, 15],
      },

      '& h3': {
        margin: [0, 0, 20, 30],
      },

      '& .filter-list': {
        display: 'block',
        listStyle: 'none',
        padding: 0,
        margin: 0,

        '& li': {
          display: 'block',
          margin: [0, 0, 10, 0],
          position: 'relative',
          padding: [0, 30, 0, 0],
          borderLeft: '2px solid #ffffff',
          transition: 'all linear .2s',


          '& a': {
            display: 'block',
            padding: [5, 20, 5, 30],
            fontSize: '14px',
            lineHeight: '20px',
            color: theme.colorDark,
            textDecoration: 'none',
            cursor: 'pointer',
            transition: 'all linear .2s',
            fontWeight: 600,
          },

          '& .remove-filter': {
            position: 'absolute',
            fontSize: '14px',
            lineHeight: '10px',
            color: theme.colorDark,
            right: 10,
            top: 11,
            transform: 'rotate(45deg)',
            display: 'none',
            padding: 0,
            margin: 0,
          },

          '&:hover': {
            borderLeftColor: '#828282',

            '& a': {
              color: '#828282',
            },

          },

          '&.active': {
            borderLeftColor: '#828282',

            '& .remove-filter': {
              display: 'block',
            },

            '& a': {
              color: '#828282',
            },

          },

        },
      },

      '&.catalog-menu-list': {
        position: 'fixed',
        top: 110,
        left: 0,
        bottom: 0,
        padding: 0,
        boxShadow: 'none',
      },
    },

    '& .content-wrapper': {
      flex: '1 1',
      position: 'relative',
      zIndex: '5',
      padding: [65, 0, 70, 0],
      boxSizing: 'border-box',

      '& .main-content-title': {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        fontSize: '18px',
        lineHeight: '25px',
        padding: [20, 20, 20, 40],
        color: theme.colorWhite,
        fontWeight: 600,
        background: theme.colorDark,
        margin: 0,

        '& .menu-btn': {
          display: 'none',
        },

      },

      '& .content-area': {
        width: '100%',
        height: '100%',
        overflowY: 'scroll',
        boxSizing: 'border-box',
        padding: [25, 40, 5, 40],
      },
    },

    '& .list-wrap': {
      '& .row': {
        alignItems: 'stretch',

        '& .item-wrap': {
          margin: [0, 0, 30, 0],
        },

        '& .item': {
          padding: [30, 20, 30],
          background: theme.colorWhite,
          height: '100%',
          boxShadow: '0px 0px 5px rgba(0, 0, 0, 0.25)',
          margin: [0, 0],
          textAlign: 'left',
          wordBreak: 'break-word',
          boxSizing: 'border-box',
          position: 'relative',

          '& .date': {
            position: 'absolute',
            top: 5,
            right: 0,
            background: theme.colorDark,
            padding: [3, 5],
            fontSize: '12px',
            lineHeight: '14px',
            color: theme.colorWhite,
          },

        },
      },
    },

    '& .page-wrap': {
      alignItems: 'center',
      position: 'fixed',
      left: 245,
      bottom: 0,
      right: 0,
      padding: [15, 40, 15, 25],
      margin: 0,
      background: '#FCFCFC',

      '& .pager': {
        textAlign: 'right',

      },
    },


  },

  '@media (max-width: 768px)': {

    App: {
      '& .sidebar': {
        position: 'fixed',
        left: -245,
        top: 0,
        bottom: 0,
        transition: 'all linear .2s',

        '& .filter-list li:hover':{
          borderLeft: '2px solid #ffffff',

          '& a':{
            color: '#333333',
          },

        },

        '& .filter-list li.active:hover':{
          borderLeft: '2px solid #828282',

          '& a':{
            color: '#828282',
          },

        },

        '&.opened': {
          left: 0,
        },

      },

      '& .content-wrapper': {
        '& .page-wrap': {
          left: 0,
          paddingRight: 25,
        },

        '& .main-content-title': {
          paddingRight: 70,

          '& .menu-btn': {
            display: 'block !important',
            position: 'absolute',
            right: 40,
            top: 0,
            bottom: 0,
            width: 20,
            height: 20,
            margin: 'auto',
            background: {
              image: 'url(' + menu + ')',
              position: 'center center',
              repeat: 'no-repeat',
              size: 'contain',
            },
          },

          '&.opened .menu-btn': {
            background: {
              image: 'url(' + cross + ')',
              position: 'center center',
              repeat: 'no-repeat',
              size: 'contain',
            },
          },

        },

        '& .list-wrap .item-wrap': {
          flex: '0 0 50%',
          maxWidth: '50%',
        },
      },
    },


  },

  '@media (max-width: 530px)': {
    App: {

      '& .content-wrapper': {

        '& .remove-filter':{
          fontSize: 20,
        },

        '& .content-area': {
          paddingLeft: 20,
          paddingRight: 20,
        },

        '& .main-content-title': {
          paddingLeft: 20,

          '& .menu-btn': {
            right: 20,
          },

        },

        '& .list-wrap .item-wrap': {
          flex: '0 0 100%',
          maxWidth: '100%',
        },

        '& .page-wrap': {
          paddingLeft: 5,
          paddingRight: 5,
        },

      },

    },

  },

  '@media (max-width: 425px)': {
    App: {

      '& .content-wrapper': {
        paddingBottom:105,

        '& .page-wrap': {

          '& .select-wrap': {
            flex: 'none',
            display: 'block',
            maxWidth: '100%',
          },

          '& .pager': {
            flex: 'none',
            display: 'block',
            textAlign: 'center',
            maxWidth: '100%',
            marginTop: 15,
          },

        },
      },

    },

  },

})
