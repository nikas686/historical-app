import React, { Component } from 'react'
import injectSheet from 'react-jss'

import Select from '../../components/Select'
import Paging from '../../components/Paging'

import data from '../../data/historical-events.json'

import { style } from './style'

class App extends Component {
  state = {
    data: data.result.event,
    currentPage: 1,
    itemPerPage: 12,
    isMenuOpened: 'closed',
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
    const number = this.props.match.params.number
    if (number !== undefined && isNaN(number) === false)
      this.setState({ currentPage: number })
  }

  handleClickOutside = event =>{
    if( this.state.isMenuOpened === 'opened'){

      let el = this.refs['menuRef']
      let elBtn = this.refs['linkRef']
      let target = event.target;

      if (el !== target && elBtn !== target && !el.contains(target)){
        this.setState({isMenuOpened : 'closed'})
      }
      
    }
  }

  handleSelectClick = num => {
    const { currentPage, data } = this.state
    this.setState({ itemPerPage: num })
    const total = data.length
    const lastPage = Math.ceil(total / num)
    if (currentPage > lastPage) {
      this.setState({ currentPage: lastPage })
      this.props.history.push('/' + lastPage)
    }
  }

  renderCountItems = () => {
    const list = [
      {
        id: 12,
        name: 12,
      },
      {
        id: 24,
        name: 24,
      },
      {
        id: 48,
        name: 48,
      },
      {
        id: 96,
        name: 96,
      },
    ]

    return (
      <Select
        onClick={this.handleSelectClick}
        options={list}
        selected={this.state.itemPerPage}
      />
    )
  }

  handleNavClick = (name, id) => {
    const { itemPerPage, data, currentPage } = this.state
    const total = data.length
    const lastPage = Math.ceil(total / itemPerPage)

    if (name === 'prev' && id > 1) {
      this.setState({ currentPage: id })
      this.props.history.push('/' + id)
    } else if (name === 'next' && lastPage >= id) {
      this.setState({ currentPage: id })
      this.props.history.push('/' + id)
    } else if (name === 'link') {
      this.setState({ currentPage: id })
      this.props.history.push('/' + id)
    }

    if (currentPage > lastPage) {
      this.setState({ currentPage: lastPage })
      this.props.history.push('/' + lastPage)
    }

  }

  renderList = list => {

    const { currentPage, itemPerPage } = this.state

    const itemMin = (currentPage - 1) * itemPerPage
    const itemMax = itemMin + itemPerPage

    return (
      <div className={'row'}>
        {list.map((item, index) => {
          if (index > itemMin - 1 && index < itemMax) {
            if (typeof item.description === 'object') {
              let prop = Object.getOwnPropertyNames(item.description)
              return (
                <div className={'item-wrap col-4'}>
                  <div className={'item'}>
                    <span className={'date'}>{item.date}</span>
                    <div className="text-wrap">{item.description[prop]}</div>
                  </div>
                </div>
              )
            } else {
              return (
                <div className={'item-wrap col-4'}>
                  <div className={'item'}>
                    <span className={'date'}>{item.date}</span>
                    <div className="text-wrap">{item.description}</div>
                  </div>
                </div>
              )
            }
          }
        })}
      </div>
    )
  }

  setFilter = (min, max, id) => {
    const { itemPerPage, currentPage } = this.state


    const filtred = data.result.event.filter(event => (event.date.split('/'))[0] >= min && (event.date.split('/'))[0] <= max)
    this.setState({ data: filtred })
    this.setState({ filter: id })
    const total = filtred.length
    const lastPage = Math.ceil(total / itemPerPage)
    if (currentPage > lastPage) {
      this.setState({ currentPage: lastPage })
      this.props.history.push('/' + lastPage)
    }
  }

  setActive = id => {
    if (this.state.filter === id) return 'active'
  }

  removeFilter = () => {
    this.setState({ data: data.result.event })
    this.setState({ filter: '' })
  }

  toggleMobile = () => {
    this.setState({ isMenuOpened: this.state.isMenuOpened === 'opened' ? 'closed' : 'opened' })
  }

  render() {
    const { classes } = this.props
    const { data, currentPage, itemPerPage, isMenuOpened } = this.state

    return (
      <div className={classes.App}>
        <div className={'sidebar ' + isMenuOpened} ref={'menuRef'}>
          <h2>Filter</h2>
          <h3>By Year</h3>
          <ul className="filter-list">
            <li className={this.setActive('filter-1')}><a id={'filter-1'}
                                                          onClick={event => this.setFilter(-1000, 0, event.target.id)}>-1000
              - 0</a><a className={'remove-filter'} onClick={() => this.removeFilter()}>+</a></li>
            <li className={this.setActive('filter-2')}><a id={'filter-2'}
                                                          onClick={event => this.setFilter(0, 1000, event.target.id)}>0
              - 1000</a><a className={'remove-filter'} onClick={() => this.removeFilter()}>+</a></li>
            <li className={this.setActive('filter-3')}><a id={'filter-3'}
                                                          onClick={event => this.setFilter(1000, 2000, event.target.id)}>1000
              - 2000</a><a className={'remove-filter'} onClick={() => this.removeFilter()}>+</a></li>
            <li className={this.setActive('filter-4')}><a id={'filter-4'}
                                                          onClick={event => this.setFilter(2000, 3000, event.target.id)}>2000
              - 3000</a><a className={'remove-filter'} onClick={() => this.removeFilter()}>+</a></li>
          </ul>

        </div>
        <div className={'content-wrapper'}>
          <p className={'main-content-title ' + isMenuOpened}>
            Historical App
            <a onClick={() => this.toggleMobile()} ref={'linkRef'} className={'menu-btn'}></a>
          </p>
          <div className={'content-area'}>
            <div className={'list-wrap'}>
              {this.renderList(data)}
            </div>
            <div className={'page-wrap row'}>
              <div className={'select-wrap col-2'}>
                {this.renderCountItems()}
              </div>
              <div className={'pager col-10'}>
                <Paging data={data} currentPage={currentPage} itemPerPage={itemPerPage} onClick={this.handleNavClick}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default injectSheet(style)(App)
